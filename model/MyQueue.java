package model;

import java.util.LinkedList;
import java.util.Queue;

import view.PopUp;
import view.ViewSimulation;

public class MyQueue extends Thread  {

	public static ViewSimulation vis;
	public boolean open;
	private int id;
	private int nrClients;
	public int waitingTime;
	private int totalNrOfClients;
	private int totalWaitingTime;
	private int totalServiceTime;
	public Queue<Client> myClients = new LinkedList<Client>();
	private PopUp pop;

	public MyQueue(int id, ViewSimulation vis) {
		this.id = id;
		this.vis = vis;
		open = true;
		nrClients = 0;
		waitingTime = 0;

		//the totals are needed so that the simulation interval is respected 
		totalNrOfClients = 0;
		totalWaitingTime = 0;
		totalServiceTime = 0;

		pop = new PopUp();
		pop.setVoid();
		pop.setInfo("Queue with id: " + this.id + "\n");
		pop.setBounds(550, 100, 368, 600);
	}

	public synchronized void addClient(Client c, ViewSimulation vis){
		myClients.add(c);
		pop.setInfo("Client " + c.getId() + " got at queue " + this.id + " at " + c.getArrivalTime() + " and waits for " + c.getServiceTime() + "\n");
		nrClients ++; //we add a client in a queue 
		totalNrOfClients ++; //we have a new client
		waitingTime = waitingTime + c.getServiceTime();
		totalWaitingTime += waitingTime;
		totalServiceTime += c.getServiceTime();
	//	vis.addToQ(c.printClient(), getQueueId());;
		notifyAll();

	}

	public synchronized void removeClient(ViewSimulation vis){
		nrClients --;
		waitingTime -= (myClients.peek()).getServiceTime();//we access the client that was at the desk, first introduced in the queue
		myClients.poll();
		vis.deleteHeadOfQ(getQueueId());
		notifyAll();
	}
	


	public void run() {
		while(true){
			try{
				while(this.nrClients == 0)
					Thread.sleep(1);
			}
			catch(Exception e){
				e.printStackTrace();
				System.out.println("waiting exception");
			}
			if(this.nrClients > 0){
				try{
					Thread.sleep((myClients.peek()).getServiceTime());
					pop.setInfo("Client " + (myClients.peek().getId()) + " is leaving queue " + this.id + "\n");
					this.removeClient(vis);
				}
				catch(Exception e){
					System.out.println("Exception queue!");
				}
			}
		}
	}

	public synchronized int getHowManyClients(){
		return nrClients;
	}

	public void openQueue(){
		open = true;
	}

	public void closeQueue(){
		open = false;
	}

	public int isOpen(){
		if (open == true)
			return 1;
		else
			return 0;
	}

	public int getQueueId() {
		return id;
	}

	public int getWaitingTime(){
		return waitingTime;
	}

	public int getTotalWaitingTime(){
		return totalWaitingTime;
	}

	public int getTotalServiceTime(){
		return totalServiceTime;
	}

	public int getTotalNrOfClients(){
		return totalNrOfClients;
	}

/*	public static void main (String args[]) {
		ViewSimulation vis = new ViewSimulation();
		//we set the desks //a test	
		vis.setQ1("D:");
		vis.setQ2("D:");
		vis.setQ3("D:");
		vis.setQ4("D:");
		vis.setQ5("D:");
		MyQueue q1 = new MyQueue(1, vis);
		MyQueue q2 = new MyQueue(2, vis);
		MyQueue q3 = new MyQueue(3, vis);
		Client c1 = new Client(0, 1, 10);
		Client c2 = new Client(15, 2, 15);
		Client c5 = new Client(23, 5, 17);
		Client c3 = new Client(10, 3, 12);
		Client c4 = new Client(18, 4, 7);
		Client c6 = new Client(12, 6, 10);
		Client c7 = new Client(11, 7, 3);
		q1.addClient(c1, vis);
		q1.addClient(c2, vis);
		q1.addClient(c5, vis);
		q2.addClient(c3, vis);
		q2.addClient(c4, vis);
		q3.addClient(c6, vis);
		q3.addClient(c7, vis);		
		q1.run();
		q2.run();

	}*/
}
