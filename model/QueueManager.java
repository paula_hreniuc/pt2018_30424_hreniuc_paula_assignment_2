package model;

import java.util.Random;

import view.PopUp;
import view.ViewSimulation;

public class QueueManager implements Runnable {
	
	private int clientId = 1;
	private int nrOfQueues;
	private int minArrival;
	private int maxArrival;
	private int minServiceTime;
	private int maxServiceTime;
	private int simMinInterval;
	private int simMaxInterval;
	private int totalNrOfClients = 0;
	private int totalWaitingTime = 0;
	private int totalServiceTime = 0;
	private int peak;
	private MyQueue[] queues;//keeping track of the queues we have
	private  ViewSimulation visual = new ViewSimulation(); //the visual representation of the queues
	
	
	public QueueManager(int minA, int maxA, int minS, int maxS, int nrQueues, int minSimT, int maxSimT) {

		this.peak = (minSimT + maxSimT)/2;
		this.minArrival = minA;
		this.maxArrival = maxA;
		this.minServiceTime = minS;
		this.maxServiceTime = maxS;
		this.nrOfQueues = nrQueues;
		this.simMinInterval = minSimT * 60 * 1000;
		this.simMaxInterval = maxSimT * 60 * 1000;
		
		//we set the desks 
		visual.setQ1("D:");
		visual.setQ2("D:");
		visual.setQ3("D:");
		visual.setQ4("D:");
		visual.setQ5("D:");

		queues = new MyQueue[nrOfQueues + 1]; //now we know how much to allocate for the array
		
	}
	public void setQueues(){
		for(int i = 1; i <= nrOfQueues; i++){
			queues[i] = new MyQueue(i, visual);
			queues[i].start();
		}
	}
	
	public double getAverageServiceTime(){
		return (totalServiceTime / totalNrOfClients);
	}

	public double getAverageWaitingTime(){
		return (totalWaitingTime / totalNrOfClients);
	}
	
	private synchronized int generateServiceTime(){
		Random r = new Random();
		int randomNum = r.nextInt((maxServiceTime - minServiceTime) + 1) + minServiceTime;
		return (randomNum * 1000);
	}

	private synchronized int generateArrivalTime(){
		Random r = new Random();
		int randomNum = r.nextInt((maxArrival - minArrival) + 1) + minArrival;
		return (randomNum * 1000);
	}
	
	private Client generateClient(){
		Client c = new Client(simMinInterval, clientId, generateServiceTime());
		clientId++;
		return c;
	}
	
	public void getTotals(){
		for(int i = 1; i <= nrOfQueues; i++){
			totalNrOfClients += queues[i].getTotalNrOfClients();
			totalWaitingTime += queues[i].getTotalWaitingTime();
			totalServiceTime += queues[i].getTotalServiceTime();
		}
	}
	
	public void report(){
		PopUp p = new PopUp();
		p.setVoid();
		p.setInfo("Average waiting time was " + (float)this.getAverageWaitingTime() / 1000 + "\n");
		p.setInfo("Average service time was " + (float)this.getAverageServiceTime() / 1000 + "\n");
		p.setInfo("The peak hour was: " + this.peak + "\n");
	}
	
	@SuppressWarnings("deprecation")
	public void run() {
		// TODO Auto-generated method stub
		this.setQueues();
		while(simMinInterval < simMaxInterval){
			int min = queues[1].getWaitingTime();
			int index = 1;
			Client c = generateClient();
			//we find the queue with the minimum waiting time
			for (int i = 2; i <= nrOfQueues; i++){
				if (queues[i].getWaitingTime() < min){
					min = queues[i].getWaitingTime();
					index = i;
				}
			}
			visual.addToQ(c.printClient(), queues[index].getQueueId());
			queues[index].addClient(c, visual);
			try{
				int tm = this.generateArrivalTime();
				simMinInterval += tm;    
				Thread.sleep(tm);
			}
			catch(Exception e){
				System.out.println("thread exception");
			}
		}
		for (int i = 1; i <= nrOfQueues; i++){
			queues[i].stop();
		}
		this.getTotals();
		this.report();
	}
		
/*	public static void main (String args[]) {
		QueueManager qm = new QueueManager(1,7,2,3,3,1,2, visual);
		qm.totalWaitingTime = 3743;
		qm.totalServiceTime = 372;
		qm.totalNrOfClients = 23;
		visual.addToQ("C11C22", 1);
		visual.deleteHeadOfQ(1);
		qm.report();
	}*/
}
