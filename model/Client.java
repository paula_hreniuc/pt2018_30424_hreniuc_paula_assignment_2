package model;

public class Client {
	private int arrivalTime;
	private int serviceTime;
	private int id;
	
	public Client(int arrival, int id, int svTime){
		arrivalTime = arrival;
		this.id = id;
		this.serviceTime = svTime;
	}
	public int getId(){
		return id;
	}
	
	public String printClient() {
		String sId = Integer.toString(getId());
		return "C" + sId;
	}
	
	public int getServiceTime(){
		return(serviceTime);
	}
	
	public int getArrivalTime(){
		return arrivalTime;
	}
	
}
