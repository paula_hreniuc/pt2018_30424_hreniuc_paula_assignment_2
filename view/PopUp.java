package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class PopUp extends JDialog {

	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();
	JTextArea txtr;
	
	public PopUp() {
		getContentPane().setLayout(new GridLayout(1, 1, 5, 3));
		setBounds(100, 100, 360, 130);
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		txtr = new JTextArea();
		contentPanel.add(txtr);
		this.setVisible(true);
	}
	
	public void setVoid(){
		txtr.setText("");
	}

	public void setInfo(String s){
		txtr.append(s);
	}
	
	/*public static void main (String args[]) {
		new PopUp();
	}*/
}
