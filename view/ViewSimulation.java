package view;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ViewSimulation extends JFrame {

	private static final long serialVersionUID = 1L;

	JFrame frame2 = new JFrame("Simulation");
	JPanel panel2 = new JPanel();

	public  JTextField q1 = new JTextField(" ");
	public  JTextField q2 = new JTextField(" ");
	public  JTextField q3 = new JTextField(" ");
	public  JTextField q4 = new JTextField(" ");
	public  JTextField q5 = new JTextField(" ");

	public ViewSimulation() {
		frame2.add(panel2);
		frame2.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame2.setResizable(false);
		frame2.setSize(700, 300);
		frame2.setVisible(true);

		initialize2();
	}

	private void initialize2() {
		panel2.setLayout(new GridLayout(5, 1, 5, 3));
		panel2.add(q1);
		panel2.add(q2);
		panel2.add(q3);
		panel2.add(q4);
		panel2.add(q5);
	}

	public void setQ1(String data) {
		q1.setText(data);
	}
	
	public void addToQ1(String client) {
		q1.setText(getQ1() + client);
	}
	
	public  String getQ1() {
		return q1.getText(); //get desk 1 state
	}
	
	public void deleteHeadOfQ1() {
		String queue1 = getQ1();
		String result ;
		String before = queue1.substring(0, 2);
		String after = queue1.substring(4);
		result = before+ after;
		q1.setText(result);
	}
	
	public void setQ2(String data) {
		q2.setText(data);
	}
	
	public void addToQ2(String client) {
		q2.setText(getQ2() + client);
	}
	
	public  String getQ2() {
		return q2.getText(); //get desk 2 state
	}
	
	public void deleteHeadOfQ2() {
		String queue2 = getQ2();
		String result ;
		String before = queue2.substring(0, 2);
		String after = queue2.substring(4);
		result = before+ after;
		q2.setText(result);
	}

	public void setQ3(String data) {
		q3.setText(data);
	}
	
	public void addToQ3(String client) {
		q3.setText(getQ3() + client);
	}
	
	public  String getQ3() {
		return q3.getText(); //get desk 3 state
	}
	
	public void deleteHeadOfQ3() {
		String queue3 = getQ3();
		String result ;
		String before = queue3.substring(0, 2);
		String after = queue3.substring(4);
		result = before + after;
		q3.setText(result);
	}

	public void setQ4(String data) {
		q4.setText(data);
	}
	
	public void addToQ4(String client) {
		q4.setText(getQ4() + client);
	}
	
	public  String getQ4() {
		return q4.getText(); //get desk 4 state
	}
	
	public void deleteHeadOfQ4() {
		String queue4 = getQ4();
		String result ;
		String before = queue4.substring(0, 2);
		String after = queue4.substring(4);
		result = before + after;
		q4.setText(result);
	}

	public void setQ5(String data) {
		q5.setText(data);
	}
	
	public void addToQ5(String client) {
		q5.setText(getQ5() + client);
	}
	
	public  String getQ5() {
		return q5.getText(); //get desk 5 state
	}
	
	public void deleteHeadOfQ5() {
		String queue5 = getQ5();
		String result ;
		String before = queue5.substring(0, 2);
		String after = queue5.substring(4);
		result = before+ after;
		q5.setText(result);
	}
	///////////////
	public void setQ(String data, int nrQ) {
		if(nrQ == 1)
			q1.setText(data);
		if(nrQ == 2)
			q2.setText(data);
		if(nrQ == 3)
			q3.setText(data);
		if(nrQ == 4)
			q4.setText(data);
		if(nrQ == 5)
			q5.setText(data);
	}
	
	public void addToQ(String client, int nrQ) {
		if(nrQ == 1)
			q1.setText(getQ1() + client);
		if(nrQ == 2)
			q2.setText(getQ2() + client);
		if(nrQ == 3)
			q3.setText(getQ3() + client);
		if(nrQ == 4)
			q4.setText(getQ4() + client);
		if(nrQ == 5)
			q5.setText(getQ5() + client);		
	}
		
	public void deleteHeadOfQ(int nrQ) {
		String queue = "";
		String before;
		String after;
		
		if(nrQ == 1)
			queue = getQ1();
		if(nrQ == 2)
			queue = getQ2();
		if(nrQ == 3)
			queue = getQ3();
		if(nrQ == 4)
			queue = getQ4();
		if(nrQ == 5)
			queue = getQ5();
		
		String result ;
		before = queue.substring(0, 2);
		if (queue.charAt(4) == 'C')
			after = queue.substring(4);
		else
			after = queue.substring(5);
		
		result = before+ after;
		
		if(nrQ == 1)
			q1.setText(result);
		if(nrQ == 2)
			q2.setText(result);
		if(nrQ == 3)
			q3.setText(result);
		if(nrQ == 4)
			q4.setText(result);
		if(nrQ == 5)
			q5.setText(result);
		
	}
	
	
	
	/*public static void main (String args[]) {
		ViewSimulation vis = new ViewSimulation();
		vis.setQ("D:C11C2C3C4", 1);
		vis.deleteHeadOfQ(1);
		vis.setQ("D:C12C3C4", 2);
		vis.deleteHeadOfQ(2);
		vis.setQ1("D:C1C2C3");
		vis.deleteHeadOfQ1();
		vis.deleteHeadOfQ1();

		vis.setQ2("D:C1C2C3");
		vis.deleteHeadOfQ2();
		vis.setQ3("D:C1C2C3");
		vis.deleteHeadOfQ3();
		vis.setQ4("D:C1C2C3");
		vis.deleteHeadOfQ4();
		vis.setQ5("D:C1C2C3");
		vis.deleteHeadOfQ5();
	}*/
	
}
