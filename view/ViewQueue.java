package view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.QueueManager;


public class ViewQueue extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	private QueueManager q;

	JFrame frame = new JFrame("Queues");
	JPanel panel = new JPanel();

	JLabel arriving = new JLabel("Arriving time between customers. Give an interval:", JLabel.LEFT);
	JLabel l1 = new JLabel("");
	JLabel minA = new JLabel("min value");
	JLabel maxA = new JLabel("max value");
	public  JTextField minArriving = new JTextField("");
	public  JTextField maxArriving = new JTextField("");

	JLabel service = new JLabel("Serivice time. Give an interval:", JLabel.LEFT);
	JLabel l2 = new JLabel("");
	JLabel minS = new JLabel("min value");
	JLabel maxS = new JLabel("max value");
	public JTextField minServiceTime = new JTextField("");
	public JTextField maxServiceTime = new JTextField("");

	JLabel queues = new JLabel ("Give the number of queues:", JLabel.LEFT);
	JLabel l3 = new JLabel("");
	public JTextField nrQueues = new JTextField("");
	JLabel l4 = new JLabel("");

	JLabel sim = new JLabel ("Simulation time. Give an interval:", JLabel.LEFT);
	JLabel l5 = new JLabel("");
	JLabel minSim = new JLabel("min value");
	JLabel maxSim = new JLabel("max value");
	public JTextField minSimulationTime = new JTextField("");
	public JTextField maxSimulationTime = new JTextField("");

	public JButton start = new JButton("START");
	public JButton stop = new JButton("STOP");
	public JLabel instructions = new JLabel("The maximum number of queues is 5 ");


	public ViewQueue() { 
		frame.add(panel);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(700, 350);
		frame.setVisible(true);

		initialize();
		start.addActionListener(new ActionListener()
		{

			public void actionPerformed(ActionEvent event){

				try{
					int aux;
					int minArrival = Integer.parseInt(getMinArrivingTime());
					System.out.println("minArrival:" + minArrival);
					int maxArrival = Integer.parseInt(getMaxArrivingTime());
					System.out.println("maxArrival:" + maxArrival);
					int minST = Integer.parseInt(getMinServiceTime());
					System.out.println("minST:" + minST);
					int maxST = Integer.parseInt(getMaxServiceTime());
					System.out.println("maxST:" + maxST);
					int nrOfQ = Integer.parseInt(getNrQueues());
					System.out.println("nrOfQ:" + nrOfQ);
					int simMinInt = Integer.parseInt(getMinSimulationTime());
					System.out.println("simMinInt:" + simMinInt);
					int simMaxInt = Integer.parseInt(getMaxSimulationTime());
					System.out.println("simMaxInt:" + simMaxInt);
					if (minArrival > maxArrival){
						aux = minArrival;
						minArrival = maxArrival;
						maxArrival = aux;
					}
					if (minST > maxST){
						aux = minST;
						minST = maxST;
						maxST = aux;
					}
					if (simMinInt > simMaxInt){
						aux = simMinInt;
						simMinInt = simMaxInt;
						simMaxInt = aux;
					}
					if (simMinInt == simMaxInt || nrOfQ > 5 || nrOfQ < 1){
						PopUp p = new PopUp();
					}
					else{
						start.setEnabled(false);
						System.out.println("starts?\n");
						q = new QueueManager(minArrival, maxArrival, minST, maxST, nrOfQ, simMinInt, simMaxInt);//, visual);
						System.out.println("nu queue");
						Thread t = new Thread(q);
						t.start();
					}
				}
				catch(Exception e){
					PopUp p = new PopUp();
					p.setInfo("wrong input");
					System.out.println(e.getMessage());
				}

			}

		});
	}

	private void initialize() {
		panel.setLayout(new GridLayout(14, 2, 5, 3));
		panel.add(arriving);
		panel.add(l1);
		panel.add(minA);
		panel.add(maxA);
		panel.add(minArriving);
		panel.add(maxArriving);
		panel.add(service);
		panel.add(l2);
		panel.add(minS);
		panel.add(maxS);
		panel.add(minServiceTime);
		panel.add(maxServiceTime);
		panel.add(queues);
		panel.add(l3);
		panel.add(nrQueues);
		panel.add(l4);
		panel.add(sim);
		panel.add(l5);
		panel.add(minSim);
		panel.add(maxSim);
		panel.add(minSimulationTime);
		panel.add(maxSimulationTime);
		panel.add(start);
		panel.add(stop);
		panel.add(instructions);


	}

	public String getMinArrivingTime() {
		return minArriving.getText();
	}

	public String getMaxArrivingTime() {
		return maxArriving.getText();
	}

	public String getMinServiceTime() {
		return minServiceTime.getText();
	}

	public String getMaxServiceTime() {
		return maxServiceTime.getText();
	}

	public String getNrQueues() {
		return nrQueues.getText();
	}

	public String getMinSimulationTime() {
		return minSimulationTime.getText();
	}

	public String getMaxSimulationTime() {
		return maxSimulationTime.getText();
	}

	public void addStartListener(ActionListener e) {
		start.addActionListener(e);
	}

	public void addStopListener(ActionListener e) {
		stop.addActionListener(e);
	}


	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
	
	/*public static void main (String args[]) {
		new ViewQueue();
	}*/

}
